Over the next year, the Manage stage will provide such compelling, must-have value to large customers that we will bring on many new users.
This means that Manage is a must-have part of the feature set that supports that customer and that Manage was a key part of their adoption journey.

### Goal Themes

#### Enterprise readiness
We're going to focus on increasing and retaining the number of customers with enterprise-scale needs. We're doing this by focusing on:

* Enterprise-grade authentication and authorization. We'll focus on SAML and build excellent compatibility and documentation with large identity providers. This should work on both GitLab.com and self-managed.
* Comprehensive audit events for everything that’s done within GitLab and allowing those events to be accessible via the API and UI.
* Isolation and control, especially for GitLab.com. For some organizations, there must be safeguards in place to prevent users from viewing or accessing other groups and projects. Providing isolation of [enterprise users](https://gitlab.com/groups/gitlab-org/-/epics/4786)] will help organizations better manage their GitLab usage by providing a more "instance-like" experience at the group level.

| Success factor | How we'll measure |
| ------ | ------ |
| Increased self-managed enterprise adoption | At least X customers over Y paid seats purchasing a self-managed license in 2021 |
| Increased GitLab.com enterprise adoption | At least 15 customers over 500 paid seats purchasing GitLab.com in 2021 |
| Increased engagement with enterprise must-have features | Increase in paid feature engagement for the enterprise: [Paid SAML](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#manageaccess---paid-gmau---mau-using-paid-saml) |
| Increased CSAT from large organizations | TBD |

##### GitLab.com
Of particular importance to our goal of enterprise readiness is ensuring that GitLab.com is ready to meet the needs of large organizations. By the end of 2021, we will close the following gaps between self-managed and GitLab.com:

| Problem | Approach |
| ------ | ------ |
| Some user events lack auditability and visibility. | We'll continue to add [additional audit events](https://about.gitlab.com/direction/manage/compliance/audit-events/#whats-next--why) in line with our prioritization framework. |
| Individual members of an organization can fork enterprise projects into their personal namespace, introducing security concerns over IP controls. | Groups can already [prevent forks](https://gitlab.com/gitlab-org/gitlab/-/issues/216987) outside their namespace. In the future, [[enterprise users](https://gitlab.com/groups/gitlab-org/-/epics/4786) will allow organizations to manage project forks, even if they're in personal namespaces. |
| Organizations want to have traceability (and control) over the actions a user's account is taking. | While we don't have plans for an organization to have control over all actions a user's account is taking (a single user account can belong to multiple paid groups; defining account ownership in GitLab.com's current architecture is very challenging), we will allow an enterprise to have [visibility and manage user activity that is directly relevant to their organization's group](https://gitlab.com/groups/gitlab-org/-/epics/4345). |
| Since GitLab, Inc. employees are the only admins on GitLab.com, administrative controls for group owners are limited. | We're tentatively planning to pull instance-level admin activity into an [administrative area for group owners](https://gitlab.com/gitlab-org/gitlab/-/issues/209020). |
| Managing credentials (e.g. regular rotation) is not possible, since users belong to the instance and not a particular namespace. | We will allow users to create group specific credentials that can be managed by administrators in a group specific version of [Credentials Inventory](https://gitlab.com/groups/gitlab-org/-/epics/4123)|
| Since instance-level functionality is admin-only, group owners can't use any features built at this level. | [Workspaces and cascading settings](https://gitlab.com/groups/gitlab-org/-/epics/4419) will allow us to build features and settings at the group-level so they are accessible in both GitLab deployment models.|

If you'd like to comment on our GitLab.com approach, please consider opening an MR or leaving a comment in [this feedback issue](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17277).

#### Enhancing Ultimate
We're going to drive an Ultimate story that creates obvious, compelling value. The majority of Ultimate's value proposition lies in application security, but we'll strive to improve the breadth of this tier by driving more value in Ultimate, such as:

* Improving tools that help compliance-minded organizations thrive. GitLab makes it easy to contribute, but administrators should have comprehensive control to establish, enforce, and provide evidence of organizational policies that are part of a compliance program or framework. Our compliance vision will evolve to introduce features that enable organizations to rely on GitLab for the enforcement and documentation of policies they set.
* More customizable and fine-grained permissions. GitLab's RBAC permissions system works well for most, but we should offer more powerful customization for customers to leverage.
* Powerful analytical insights. Provide dashboarding and analytics for project and portfolio management, allowing business to track and communicate progress on work in flight, capacity of teams and projects, and overall efficiency across their full portfolio.

Success in this theme looks like:
* Increased share of IACV in Ultimate.
* Increased adoption and engagement with Ultimate-level features.

#### Easy adoption
Manage will create easy paths to support our land-and-expand strategy. There's a starting point for any organization with an expansive new tool, and Manage will make this transition easy by supporting natural starting points - ideally in Core, for all groups - that get our customers started and hooked on GitLab:
* Easier import at any scale. Large-scale moves to GitLab should be significantly easier. We'll particularly focus on the user experience migrating from 2-3 key competitors, including gracefully recovering from failures.
* Drive entry-level enterprise table stakes into Core. Each group will focus on a Core value proposition that allows every user to get value - and encourages enterprises testing the water to land (and later expand) in GitLab.

### Access
1. [Flexible Roles and Permissions](https://gitlab.com/groups/gitlab-org/-/epics/4035) are top of mind for customers. The pre-defined 5 roles that GitLab [currently provides](https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions) are not flexible enough for the needs of today's enterprise: some roles are too permissive and others are not permissive enough. We want to enable our customers to define their own roles and the permissions that come along with each role, so that they can find their perfect balance between security and ease of use.
2. [Enterprise Users](https://gitlab.com/groups/gitlab-org/-/epics/5299) - As customers adopt GitLab, they want to retain control over their company's users and what they can and can't do in GitLab. They also have an interest in protecting their inetellectual property. To accomplish this, we will enable enterprises to establish ownership of their [domains](https://gitlab.com/gitlab-org/gitlab/-/issues/321763). From there, they can then [claim users](https://gitlab.com/gitlab-org/gitlab/-/issues/322039) which fall under that domain. Once users are claimed, administrators can customize workflows to reduce friction for enterprise owned users.
3. Add the ability to create and use [service accounts](https://gitlab.com/gitlab-org/gitlab/-/issues/284393), allowing automated processes to act on behalf of users without consuming a license seat.
4. [Workspace](https://docs.gitlab.com/ee/user/workspace/#workspace) is being created as the top level namespace - a very important place in the application for GitLab Administrators, since this is where many top level settings and user management resides. Currently, the lack of top level container on GitLab.com makes for a discrepancy on the administrator controls available on GitLab.com vs self-hosted. Our team will work to close this gap - as Workspace is created, we will migrate Administrator functionality to the new container. 

Access uses a [board](https://gitlab.com/groups/gitlab-org/-/boards/3313904?scope=all&label_name[]=group%3A%3Aaccess&not[label_name][]=security&not[label_name][]=infradev) that is a reflection of our roadmap, divided approximately by quarters. If you're not confident an important Access issue is on our roadmap, please feel free to highlight by commenting in the relevant issue and @ mentioning hsutor.

### Optimize
In order to reach our vision, we need to:
There are many different analytics scattered throughout GitLab that bring lots of insights and observability into your DevOps processes. In this year we will focus on:
* Value Stream Analytics - which allow you to map streams and finds bottle-necks in your process.
* DevOps reports - This allows you to track how and which teams and projects are utilizing different functionality in the DevOps process, which will help you learn from high performing teams and identify projects that are not following predefined practices. 

### Compliance
Towards this vision, the Compliance group at GitLab is focused on three key areas:
* Enabling organizations to define controls and policies, such as separation of duties, in a single location and easily apply and enforce them on the groups and projects that need it.
* Provide visiblity into what is happening in GitLab through a comprehensive audit event system that can be used either directly inside GitLab or exported into third-party tools.
* Delivering a "single pane of glass" to enable compliance teams to quickly identify non-compliant activities throughout the organization so they can take action.

### Import
Following the vision laid out in our [roadmap](https://gitlab.com/gitlab-com/Product/-/issues/2680), the Import group is focused on these goals for 2022:
* First-class GitLab-to-GitLab migration experience. Allow an easy self-serviced migration of Groups and Projects from a self-managed instance to GitLab.com and vice versa.
* Reliable and scaleable imports from GitHub. Support the import of all relevant GitHub concepts for projects of all sizes.
